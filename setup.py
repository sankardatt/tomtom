from setuptools import setup

setup(
    name='geoclustering',
    version='0.1.0',    
    description='Python package for cartographical operations',
    url='https://github.com/sankardatt/',
    author='Sankar Datta',
    author_email='sankardatt@gmail.com',
    packages=['geoclustering'],
    install_requires=[
        'pytest',
    ], 

    classifiers=[
        'Development Status :: 1 - POC',
        'Intended Audience :: Science/Research',
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.8',
    ],
)
