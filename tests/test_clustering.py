import pytest
from decimal import Decimal
from geoclustering.clustering import Map, Screen, GeoLocation, ScreenLocation


@pytest.mark.parametrize(
       "screen, zoom, cluster_number",
        [
            (Screen(800, 800), 10, 19),
            (Screen(800, 800), 20, 12),
            (Screen(400, 400), 10, 11),
            (Screen(400, 400), 20, 28),
            (Screen(200, 200), 10, 7),
            (Screen(200, 200), 20, 51),
        ],
)
def test_it_clustering_depends_on_screen_and_zoom(screen, zoom, cluster_number):
    map_obj = Map(screen, zoom)
    assert len(map_obj.clusters) == cluster_number


@pytest.mark.parametrize(
       "screen, zoom, cluster_number",
        [
            (Screen(800, 800), 10, 19),
            (Screen(800, 800), 20, 12),
            (Screen(400, 400), 10, 11),
            (Screen(400, 400), 20, 28),
            (Screen(200, 200), 10, 7),
            (Screen(200, 200), 20, 51),
        ],
)
def test_total_number_of_locations_are_same(screen, zoom, cluster_number):
    map_obj = Map(screen, zoom)
    
    locations = 0
    for cluster in map_obj.clusters:
        locations = locations + len(cluster)
    locations = locations + len(map_obj.stand_alone_locs)
    
    assert  locations == len(map_obj.locations)


@pytest.mark.parametrize(
       "screen, zoom, location1, location2, is_overlapping",
        [
            (
                Screen(800, 800),
                10,
                GeoLocation(
                    Decimal(51.485337),
                    Decimal(0.107741)
                ),
                GeoLocation(
                    Decimal(51.485424),
                    Decimal(0.10558)
                ),
                True
            ),
            (
                Screen(800, 800),
                20,
                GeoLocation(
                    Decimal(51.485337),
                    Decimal(0.107741)
                ),
                GeoLocation(
                    Decimal(51.485424),
                    Decimal(0.10558)
                ),
                False
            ),
        ],
)
def test_it_finds_if_point_are_overlapping(screen, zoom, location1, location2, is_overlapping):
    screen_loc_1 = ScreenLocation(location1, zoom, screen, 1)
    screen_loc_2 = ScreenLocation(location2, zoom, screen, 1)
    assert screen_loc_1.is_overlapping(screen_loc_2) == is_overlapping

