# Implementation of a Clustering Library

## Getting Started


#### Installation

The project is built on python3.8 and is not tested on other versions. It is recommended
to use python3.8 to run or test the algorithm.

The package can be easily installed locally by 
```
cd /path/to/the/project/directory
pip install .
```

#### Usage

You can import the `minimum` function to use it:

```
from geoclustering.clustering import Map, Screen

screen = Screen(800, 600)
zoom = 15
map_obj = Map(screen, zoom)

clusters_of_locations = map_obj.clusters
single_locations = map_obj.stand_alone_locs
```
 
#### Tests

You can run the tests by using:

``` 
tox
```

For tox to work, you will need tox installed. To install tox you can:

```
pip install tox
```

## Description

### System Design

Considering the nature of the web app, we can safely assume that it is widely used in mobile or mobile-like devices. Such devices are generally limited by their software, hardware or even their network capabilities. This enforces the need of using the network and the user device optimally to provide the best experience. Two of the major concerns will be the expensive clustering algorithm and the network. This is why, how we design our system will have a subtantial impact on the performance of the entire system.
This raises the question, where to perform the clustering algorithm for optimal performance:

Points to consider to make a decision:

  - User expectations and its costs:
    - For a user, after loading the map, moving around the map is the obvious next step whereas for the system it means recomputing everything. Using a network call and then rendering the map again might be too expensive and repetitive. Even more so, if it is a location of poor network.
    - When a user taps on a cluster, the user expects to see all the locations in the cluster instantaneously. Clusters should have access to the underlying data points without a network call. Clustering, thus, will not reduce the amount of data transmitted/stored.  
   
  - Some parameters that can influence decision making:
    - Average clustering time on user device vs on server.
    - If offline usage is supported.
    - Network characteristics and availability of fallback networks.
    - Properties (e.g data volume) of the location points.
    - If there is a need to send all data points to device simultaneously.

  - Clustering in the back-end:
    - Clustering in back-end obviously has the benefit of using the best hardware/software possible.
    - Updating the background algorithms is relatively easier and has less possibility of causing front-end bugs.
    - We can leverage heavy weight libraries like scikit, numpy etc.
    - User devices are relieved of the expensive computations.
    - System can cache the results/pre-compute the clusters. Can save computations when same request comes from different users.
    - Analizing performance will be comparatively easy.
    - *However*, re-rendering the map will take longer time as network call is involved.
    - User devices are heavily dependent on the network.

  - Clustering in the front end:
    - It may provide a better user experience if we store the data points in the device proactively, specially in a bad network.
    - If the device/algorithm can compute fast enough and the data points are readily available user interface will react fast providing comfortable experience.
    - It relieves the back-end servers from the expensive computations for each request (parallelizing the computations).
    - *However*, clustering in the users device may take longer to computational time (dependent on the device computational power).
    - Updating the algorithms needs more testing due to browser/platform compatibility issues. There will be a need to actively support different platforms.
    - Analizing performance will be comparatively difficult.
    - Running an expensive clustering algorithm along with loading all the data on a user device may slow the device down.


Conclusion:
 We may need to find a sweet spot for trade off between front end and back end that would provide us the strengths of both side and reduce their limitations. Something that we didn't consider so far may offer us a hint. For a human it is irrelevant (or even unpleasant) if we render all the data points simultaneously. If the data points are rendered one after another, it may help the user to focus on them progressively, provided the data points are loaded within an acceptable time.

```
 An average person can keep only 7(+/-2) items in their working memory at a time.
 - Miller's Law
``` 

  Hence, it can be proposed that, we run the clustering algorithm in the back-end server and serve the data to the user device progressively in batches. The device can start rendering the locations on the map while it is waiting for the network to respond with the next batch of locations/clusters. The algorithm in this project (and not the only algorithm) computes the clusters in order of largest to smallest, which is why, the clusters/locations can also be sent to the web app on demand, in batches.
  If we use this approach, we can even pre-compute clusters and store them on the server and transmit them upon request. We can come to solutions where we can compute the clusters once and serve them multiple times for requests from different users. This can be useful for popular areas with stationary location points e.g. restaurants.

#### Further Possible Improvements:

  - When the map is already loaded, and the user moves the map, we do not need to re-render the locations that are already loaded. We can just load the new area. 
  - If we load the data not only for the exact area on the map but an extended area of the user's focus (e.g. when the user is looking at 20 sq.km if we already load the data for 30 sqkm) the user can move around and the clustering will be provided to the user almost instantaneously.
  - Would it be possible to prioritize data points/clusters with certain parameters e.g. popularity, distance from current location etc?


### Clustering Algorithm

There are many density based clustering algorithms available e.g. DBSCAN, K-Means etc. However, our need dictates to develop an algorithm that can cluster the data points progressively. Along with that, we would like a clustering algorithm that is limited to produce convex clusters. An arbitrary shape of cluster, e.g. a ring, is counter productive. 
Another question we may need to answer ourselves:

  - if we want to create the least number of clusters so that user gets maximum stand alone data points (thus reducing the number of clicks for the user at the cost of a more cluttered interface)
  - Or, is our approach to create as many clusters as possible (thus creating a clean interface with the cost of additional user clicks).

If we chose our icon size and the overlapping area properly, the first approach will reduce the number of clicks for a user and not generate a cluttered interface. This is why the current approach can be expected to produce the least number of clusters.

Explanation:
 This clustering algorithm, iterates over the data points to find all the overlapping data points for each of the data points. Now it selects the data point with most number of overlapping data points and clusters them together. The process repeats until there are no more overlapping data points. The complexity of the approach is O(N<sup>2</sup>). The existing algorithm can be modified with little effort to serve data points in batches. 


### UI

While designing a UI, few key rules to focus:

  - Reduce the complexity for the users.
  - Maintaining similarity with other websites/applications.
  - Keeping the targets as much as possible within the of reach of the users and easy to tap/click.

the list goes on... Refer to O'Reilly `Laws of UX by Jon Yablonski` for a good read.

A user will not use this app in the most comforatable situations. This app is expected to be used while driving, riding bike, sometimes with one hand. The UI should be built considering these conditions.

#### Visualization of Clusters

A general way to visualize a cluster is to show the number of data points in a two digit format. This format can be utilized as this is something users are familiar with and it is also intuitive. This will reduce the complexity for a user and maintain a similarity with other platforms.

#### User Interaction with the Cluster
  Tapping on the cluster should show the list of locations on the side of the map. While it is in the tapped state, it can show the locations with dots/colored pixels in the cluster with one focus color (and the cluster itself becomes transparent). This way, the locations can be approximated by the users without cluttering the interface. The list will bring the target well within the reach of the user and the comparatively large size of the list items make them easy to click/tap.


















