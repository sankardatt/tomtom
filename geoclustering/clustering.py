import csv
import logging
from decimal import Decimal
from math import pi, sin, cos, atan2, log, tan


logger = logging.getLogger(__name__)


PI = round(Decimal(pi), 10)


class PointIcon:
    ''' Represents the icon on the display
    '''

    def __init__(self):
        self.height = 40
        self.width = 40


class Screen:
    ''' Represents the display screen
    '''

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class GeoLocation:
    ''' Represents the world location of a data point
    '''

    def __init__(self, lattitude: Decimal, longitude: Decimal):
        self.lattitude = lattitude * PI/180
        self.longitude = longitude * PI/180

    def x(self, zoom, screen_width):
        return (screen_width/PI) * (2 ** zoom) * (self.longitude + PI)
    
    def y(self, zoom, screen_height):
        return (
            (screen_height/PI) * (2 ** zoom)
            * (PI - Decimal(log(tan(self.lattitude/2 + PI/4)))) 
        )

    def __str__(self):
        return f'GeoLocation- Lat: {self.lattitude} Long: {self.longitude}'
    
    def __repr__(self):
        return str(self) 


class ScreenLocation:
    ''' Represents the location on the screen
    '''

    def __init__(self, location: GeoLocation, zoom: int, screen:Screen,  idn: int):
        self.location = location
        self.id = idn
        self.zoom = zoom
        self.screen = screen
        self.point_icon = PointIcon()
        self.x = (screen.x/PI) * (2 ** zoom) * (location.longitude + PI)
        self.y = (
            (screen.y/PI) * (2 ** zoom)
            * (PI - Decimal(log(tan(location.lattitude/2 + PI/4))))
        )


    def is_overlapping(self, other):
        if abs(self.x - other.x) < self.point_icon.width:
            return True
        
        if abs(self.y - other.y) < self.point_icon.height:
            return True
        
        return False

    def __str__(self):
        return f'ScreenLocation: id: {self.id} Location: [{self.x} {self.y}]'
    
    def __repr__(self):
        return str(self) 


class Map:
    ''' Represents the map in view of the user
    '''

    def __init__(
            self,
            screen: Screen,
            zoom: int
        ):
        self.zoom = zoom
        self.screen = screen
        self.locations = {}
        self.__tmp_locations = {}
        self._clusters = []
        self.load_locations()
        try:
            self._clusterize()
        except Exception:
            log.exception('Unable to clusterize data')
            raise

    @property
    def stand_alone_locs(self):
        return [self.locations[key] for key in self.__tmp_locations.keys()]

    @property
    def clusters(self):
        return self._clusters

    def load_locations(self):
        # Here we can load it from db but for the
        # moment we do it from the test data.

        try:
            with open('example data-set.csv', 'r') as f:
                locations = csv.reader(f, delimiter=',', quotechar='"')
                next(locations)
                for location in locations:
                    geo_loc = GeoLocation(
                        Decimal(location[1]),
                        Decimal(location[2]),
                    )
                    screen_loc = ScreenLocation(
                        location=geo_loc,
                        idn=location[0],
                        zoom=self.zoom,
                        screen=self.screen
                    )
                    self.locations[location[0]] = screen_loc
                    self.__tmp_locations[location[0]] = {
                        'screen_location': screen_loc,
                        'overlapping_locs': [],
                    }
        except Exception:
            log.exception('Error occured while retrieving data')


    def _clusterize(self):
        clusters = {}
        another_run_reqd = False

        if self.zoom == 1:
            # Minimum zoom condition
            return
        
        # Find the overlaps per location point
        for id_main, main_loc in self.__tmp_locations.items():
            self.__tmp_locations[id_main]['overlapping_locs'] = []
            for id_sec, secondary_loc in self.__tmp_locations.items():
                if id_main == id_sec:
                    continue
                
                if main_loc['screen_location'].is_overlapping(
                        secondary_loc['screen_location']
                ):
                    another_run_reqd = True
                    self.__tmp_locations[id_main][
                        'overlapping_locs'
                    ].append(id_sec)

        # Find the one with most number of overlaps
        max_overlaps = 0
        heaviest_loc = 0
        for idn, loc in self.__tmp_locations.items():
            lgth = len(loc['overlapping_locs'])
            if lgth > max_overlaps:
                max_overlaps = lgth
                heaviest_loc = idn

        if max_overlaps == 0:
            return

        # Cluster all these overlapping ones
        cluster_points = self.__tmp_locations[heaviest_loc][
            'overlapping_locs'
        ].copy()
        cluster_points.append(heaviest_loc)
        self._clusters.append([self.locations[i] for i in cluster_points])
        
        # Don't forget to remove them from the locations point
        for idn in cluster_points:
            self.__tmp_locations.pop(idn)

        if another_run_reqd:
            return self._clusterize()
        
        return 

